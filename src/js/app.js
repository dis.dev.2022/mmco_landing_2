"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import Dropdown from "./components/dropdown.js";
import Spoilers from "./components/spoilers.js";
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery
import {WebpMachine} from "webp-hero"

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// IE Webp Support
const webpMachine = new WebpMachine();
webpMachine.polyfillDocument();


// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// Меню для мобильной версии
mobileNav();

// Spoilers
Spoilers();

// Dropdown
Dropdown();

// Modal Fancybox
Fancybox.bind('[data-fancybox]', {
    autoFocus: false
});

// Sliders
import "./components/sliders.js";

document.addEventListener('scroll', (event) => {
    const header = document.querySelector('[data-header]')
    const sticky = document.querySelector('.header-sticky')

    if (sticky.getBoundingClientRect().top < 0) {
        header.classList.add("header--fixed");
    } else {
        header.classList.remove("header--fixed");
    }
})


window.addEventListener("load", function (e) {

});
