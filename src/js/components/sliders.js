/*
Документация по работе в шаблоне:
Документация слайдера: https://swiperjs.com/
Сниппет(HTML): swiper
*/

import Swiper, {Autoplay, Navigation, Pagination, EffectFade} from 'swiper';

// Инициализация слайдеров
function initSliders() {
    // Перечень слайдеров
    if (document.querySelector('[data-swiper]')) {
        new Swiper('[data-swiper]', {
            // Подключаем модули слайдера
            // для конкретного случая
            //modules: [Navigation, Pagination],
            /*
            effect: 'fade',
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
            */
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 0,
            autoHeight: true,
            speed: 800,
            //touchRatio: 0,
            //simulateTouch: false,
            //loop: true,
            //preloadImages: false,
            //lazy: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            // Arrows
            navigation: {
                nextEl: '.swiper__more .swiper__more--next',
                prevEl: '.swiper__more .swiper__more--prev',
            },
            breakpoints: {
                640: {
                    slidesPerView: 2,
                    spaceBetween: 16,
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 24,
                },
                1024: {
                    slidesPerView: 4,
                    spaceBetween: 32,
                },
            },
            on: {}
        });
    }

    if (document.querySelector('[data-evolution]')) {
        const evo = new Swiper('[data-evolution]', {
            modules: [Navigation,Autoplay, EffectFade],
            slidesPerView: 1,
            spaceBetween: 30,
            simulateTouch: false,
            autoplay: {
                delay: 10000,
                disableOnInteraction: true
            },
            navigation: {
                nextEl: '[data-evolution-next]',
                prevEl: '[data-evolution-prev]',
            },
            breakpoints: {
            },
            on: {
                slideChange: function () {
                    const evoArray = document.querySelectorAll('[data-evolution-nav]')
                    evoArray.forEach(elem => {
                        elem.classList.remove('active');
                    });
                    document.querySelector(`[data-evolution-nav="${evo.realIndex}"]`).classList.add('active');
                },
            }
        });

        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-evolution-nav]')) {
                const evoNav = event.target.closest('[data-evolution-nav]');
                const evoArray = document.querySelectorAll('[data-evolution-nav]')
                const evoSlide = parseInt(evoNav.dataset.evolutionNav)

                evoArray.forEach(elem => {
                    elem.classList.remove('active');
                });
                evoNav.classList.add('active')
                evo.slideTo(evoSlide, 1000)
            }
        })
    }

    if (document.querySelector('[data-video]')) {
        new Swiper('[data-video]', {
            slidesPerView: 'auto',
            spaceBetween: 0,
        });
    }
}

window.addEventListener("load", function (e) {
    // Запуск инициализации слайдеров
    initSliders();
});
